# dotnet-builder

A docker image for building Linux dotnet core apps that still require mono to
build. Based very, very directly off the excellent work by [ninjarobot](https://github.com/ninjarobot/docker-fsharp-mono-netcore).
